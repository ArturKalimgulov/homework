# Bank
The project is designed and developed to demonstrate working Typescript with React using Redux, Redux-saga and Ant Design.
The project allows the user to Sign Up/Login profile. Also provides Open, View, Close and make a Transactions with the Accounts (Replenish, Transfers, Payments). You can create Payment Templates, and see the Account Statement. The project also allows user to change profile (Profile Picture, First Name, Email, Password).

# Libraries
Typescript<br/>
React<br/>
Redux<br/>
Redux-saga<br/>
Redux-form<br/>
Ant Design<br/>
axios.<br/>
reselect<br/>
immer.<br/>
moment<br/>
classnames<br/>
json web token<br/>
