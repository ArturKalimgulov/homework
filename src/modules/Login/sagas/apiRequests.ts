import axios from 'axios';
import { IFormSignUpProps, IFormLoginProps, userStore } from '../types/loginTypes';

export const loginApiRequest = (email:string, password:string): Promise<IFormLoginProps> => {
    return axios.post("/login", 
    {
        email,
        password
    })
}
export const signupApiRequest = (email:string, firstName:string, password:string): Promise<IFormSignUpProps> => {
    return axios.post("/register", 
    {
        email,
        password,
        firstName
    })
}
export const getUserApiRequest = (userId:string | null): Promise<userStore> => {
    return axios.get(`/getUser/${userId}`) 
}
export const setUserModeratedApiRequest = (userId:string | null): Promise<userStore> => {
    return axios.get(`/makeClient/${userId}`)
}




