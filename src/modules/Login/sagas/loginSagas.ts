import { take, fork, call, cancel, put, delay } from 'redux-saga/effects';
import {loginApiRequest} from './apiRequests'
import { LOGIN_ISAUTHLOADER, LOGIN_ERROR, loginSuccess, loginError } from '../actions'
import {message} from 'antd';
import { IFormLoginProps } from '../types/loginTypes';

function logout () {
  localStorage.removeItem('token');
  localStorage.removeItem('userId');
}

function* loginFlow ({ email, password }: IFormLoginProps) {
  let response
  try {
    message.loading('Ожидайте', 0)
    response = yield call(loginApiRequest, email, password);
    
    const { data: { userId, token }} = response;
    
    yield localStorage.setItem('token', token);
    yield localStorage.setItem('userId', userId);
    yield delay(1000);
    yield put(loginSuccess());
    message.destroy();
    message.success('Вход выполнен', 1.5)
  } catch (error) {
    yield put(loginError());
    message.destroy();
    message.error(`${error.response?.data?.message}`, 2.5);
  }
}

function* loginSaga () {
  while (true) {
    const { payload } = yield take(LOGIN_ISAUTHLOADER);
    const login = yield fork(loginFlow, payload);
    yield take([LOGIN_ERROR]);
    yield cancel(login);
    yield call(logout);
  }
}

export default loginSaga
