import { call, put, take, delay } from 'redux-saga/effects';
import {signupApiRequest} from './apiRequests';
import { SIGNUP_ISAUTHLOADER, signupSuccess, signupError} from '../actions';
import {message} from 'antd'
import { IFormSignUpProps } from '../types/loginTypes';

function* signupFlow({ email, firstName, password }: IFormSignUpProps) {
  try {
    message.loading('Ожидайте',0);
    yield call(signupApiRequest, email, firstName, password);
    yield delay(2000);
    yield put(signupSuccess());
    message.destroy();
    message.success('Регистрация прошла успешно!',1)
  } catch (error) {
    yield put(signupError());
    message.destroy();
    message.error(`${error.response?.data?.message}`,2.5);
  }
}


function* signupSagas () {
  while (true){
    const { payload } = yield take(SIGNUP_ISAUTHLOADER);
    yield call(signupFlow, payload);
  }
}

export default signupSagas
