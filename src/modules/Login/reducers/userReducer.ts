import produce from 'immer';
import {
    GET_USER,
    GET_USER_SUCCESS,
    GET_USER_ERROR,
    USER_MODERATION_ERROR,
    USER_MODERATION_SUCCESS,
    USER_MODERATION_LOADER
  } from '../actions';
import * as types from '../types/loginTypes'
import { LoginActions } from '../actionTypes';
  

const initialState: types.userStore = {
    userId: null,
    email: null,
    firstName: null,
    isClient: false,
    profilePicture: null,
    status: false,
    moderationLoader: false,
    moderationSuccess: false
};

export default (state = initialState, action: LoginActions) => {
    return produce(state, draft => {
        if (action.type === GET_USER){
            draft.email = null;
            draft.isClient = false;
            draft.status = false;
        }
        if (action.type === GET_USER_SUCCESS){
            draft.email = action.payload.email;
            draft.firstName = action.payload.firstName;
            draft.isClient = action.payload.isClient;
            draft.profilePicture = action.payload.profilePicture;
            draft.status = action.payload.status;
        }
        if (action.type === GET_USER_ERROR){
            draft.userId = null;
            draft.email = null;
            draft.isClient = false;
            draft.status = false;
            draft.profilePicture = null;
        }
        if (action.type === USER_MODERATION_LOADER){
            draft.moderationLoader = true;
            draft.moderationSuccess = false;
        }
        if (action.type === USER_MODERATION_SUCCESS){
            draft.moderationLoader = false;
            draft.moderationSuccess = true;
            draft.isClient = true;
        }
        if (action.type === USER_MODERATION_ERROR){
            draft.moderationLoader = false;
            draft.moderationSuccess = false;
            draft.isClient = false;
        }
    })
}