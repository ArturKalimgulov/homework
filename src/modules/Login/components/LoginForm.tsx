import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import { Button } from "antd";
import { AInput, AInputPassword, FormItemItemLayout } from "../../../common/combineAntd";
import { email, required } from '../validators/loginValidators'
import * as types from '../types/loginTypes';


const LoginForm: React.FC<InjectedFormProps<types.IFormLoginProps, types.IFormProps> & types.IFormProps> = (props: any) => {
  const { handleSubmit, pristine, submitting, callback } = props;

  return (
    <form onSubmit={handleSubmit}>
      <Field label="" name="email" validate={[email, required]} component={AInput} placeholder="Email" hasFeedback/>
      <Field label="" name="password" validate={[required]} component={AInputPassword} placeholder="Пароль" hasFeedback/>
      <FormItemItemLayout>
        <Button type="primary" disabled={pristine || submitting} htmlType="submit">
          Войти
        </Button>
      </FormItemItemLayout>
      <FormItemItemLayout>
        <Button type="dashed" htmlType="button" onClick={callback}>
          Регистрация
        </Button>
      </FormItemItemLayout>
    </form>
  );
};


export default React.memo(reduxForm<types.IFormLoginProps, types.IFormProps>({form: 'login'})(LoginForm))
