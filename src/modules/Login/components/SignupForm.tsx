import React from 'react';
import { reduxForm, Field, InjectedFormProps } from "redux-form";
import { AInput, AInputPassword, FormItemItemLayout } from "../../../common/combineAntd";
import { Button } from "antd";
import { email, required, minLength3, minLength4 } from '../validators/loginValidators'
import { RollbackOutlined } from '@ant-design/icons';
import * as types from '../types/loginTypes';

const SignupForm: React.FC<InjectedFormProps<types.IFormSignUpProps, types.IFormProps> & types.IFormProps> = (props: any) => {
    const { handleSubmit, pristine, submitting, callback } = props;
    return (
        <form onSubmit={handleSubmit}>
            <FormItemItemLayout>
                <Button type="dashed" htmlType="button" onClick={callback} icon={<RollbackOutlined />}/>
            </FormItemItemLayout>
            <Field label="" name="email" validate={[required, email]} component={AInput} placeholder="Email адрес" hasFeedback/>
            <Field label="" name="firstName" validate={[required, minLength3]} component={AInput} placeholder="Ваше имя" hasFeedback/>
            <Field label="" name="password" validate={[required, minLength4]} component={AInputPassword} placeholder="Пароль" hasFeedback/>
            <Field label="" name="confirmPassword" validate={[required, minLength4]} component={AInputPassword} placeholder="Подтвердите пароль" hasFeedback/>
            <FormItemItemLayout>
                <Button type="primary" disabled={pristine || submitting} htmlType="submit">
                Отправить
                </Button>
            </FormItemItemLayout>
        </form>
    );
}

export default React.memo(reduxForm<types.IFormSignUpProps, types.IFormProps>({form: 'registration'})(SignupForm))
  
    