import {createSelector} from 'reselect';
import * as types from '../types/loginTypes'

const signup = (state: { signup: types.signUpStore }) => state.signup;  
const login = (state: { login: types.loginStore }) => state.login;  
const user = (state: { user: types.userStore }) => state.user;  

export const isSignupSuccess = createSelector(signup, signup => signup.isSuccess);
export const isSignupShow = createSelector(signup, signup => signup.isShow);
export const isLoginSuccess = createSelector(login, login => login.isAuth);
export const isLoginLoader = createSelector(login, login => login.isAuthLoader);
export const isUserModerated = createSelector(user, user => user.isClient);