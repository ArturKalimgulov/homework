import * as actions from '../actions/'
import * as types from '../types/loginTypes'

export interface signupRequestAction {
  type: typeof actions.SIGNUP_ISAUTHLOADER;
  payload: types.IFormSignUpProps;
}

export interface loginRequestAction {
  type: typeof actions.LOGIN_ISAUTHLOADER;
  payload: types.IFormLoginProps;
}

export interface onlyTypeAction {
  type: 
    typeof actions.LOGIN_SUCCESS | typeof actions.LOGIN_ERROR 
  | typeof actions.SIGNUP_SUCCESS | typeof actions.SIGNUP_ERROR 
  | typeof actions.SIGNUP_SHOW | typeof actions.SIGNUP_HIDE 
  | typeof actions.USER_MODERATION_LOADER | typeof actions.USER_MODERATION_SUCCESS
  | typeof actions.USER_MODERATION_ERROR | typeof actions.GET_USER 
  | typeof actions.GET_USER_ERROR;  
}


export interface getUserErrorAction {
    type: typeof actions.GET_USER_SUCCESS;
    payload: types.userStore;
  }
  

export type LoginActions =
  | signupRequestAction
  | loginRequestAction
  | onlyTypeAction
  | getUserErrorAction;
