import React from 'react';
import {useDispatch, useSelector, shallowEqual} from 'react-redux';
import LoginForm from '../components/LoginForm';
import SignupForm from '../components/SignupForm';
import { loginRequest, signupRequest, signupShow, signupHide } from '../actions';
import { isSignupShow } from '../selectors/loginSelectors'
import { message } from 'antd';
import * as types from '../types/loginTypes';

const LoginContainer: React.FC = () => {
        const dispatch = useDispatch();
        const isSignUpShow = useSelector(isSignupShow, shallowEqual);
    
        const loginSubmit = (values: types.IFormLoginProps) => {
            dispatch(loginRequest(values));
        }
        const signUpSubmit = (values: types.IFormSignUpProps) => {
            const { password, confirmPassword } = values;
            if (password === confirmPassword)
                dispatch(signupRequest(values));
            else message.warning('Пароли не совпадают', 1.5);
        }
        const showSignUp = () => {
            dispatch(signupShow());
        }
        const backButton = () => {
            dispatch(signupHide());
        }
        return (
            <div className="LoginPage">
                {(!isSignUpShow) && <LoginForm callback={showSignUp} onSubmit={loginSubmit}/>} 
                {(isSignUpShow) && <SignupForm callback={backButton} onSubmit={signUpSubmit}/>} 
            </div>  
        )
}

export default React.memo(LoginContainer)
