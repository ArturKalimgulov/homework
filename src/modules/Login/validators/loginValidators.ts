  export const required = (value: string) => value ? undefined : 'Обязательное';
  export const minLength = (min: number) => (value: string) =>
  value && value.length < min ? `Минимум ${min} символа` : undefined
  export const minLength3 = minLength(3);
  export const minLength4 = minLength(4);
  export const email = (value: string) =>
    value && !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(value) ?
    'Неверный формат Email' : undefined