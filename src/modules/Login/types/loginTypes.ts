export interface IFormLoginProps {
    email: string,
    password: string,
}
export interface IFormSignUpProps {
    email: string,
    firstName: string,
    password: string,
    confirmPassword: string,
}
  
export interface IFormProps {
    readonly callback: () => void;
}
  
export interface userStore {
    email: string | null,
    firstName: string | null,
    userId: string | null,
    isClient: boolean,
    profilePicture: string | null,
    status: boolean,
    moderationLoader: boolean,
    moderationSuccess: boolean
}

export interface loginStore {
    isAuth: boolean,
    isAuthLoader: boolean,
}

export interface signUpStore {
    isShow: boolean,
    isAuthLoader: boolean,
    isSuccess: boolean,
}