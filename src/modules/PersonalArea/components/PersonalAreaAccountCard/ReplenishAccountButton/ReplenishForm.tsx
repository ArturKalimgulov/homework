import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import { AInput } from "../../../../../common/combineAntd";
import { required, number, normalizeValue } from '../../../validators/PersonalAreaValidators'
import { Button } from 'antd';
import { IReplenishAccount, ILoaderToForm } from "../../../types/personalAreaTypes";

const ReplenishForm: React.FC<InjectedFormProps<IReplenishAccount, ILoaderToForm> & ILoaderToForm> = (props) => {
  const { handleSubmit, pristine, submitting, loader } = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field label="" name="value" validate={[required, number]} normalize={normalizeValue} component={AInput} placeholder="Введите сумму пополнения" hasFeedback/>
      <Button type="primary" disabled={pristine || submitting} loading={loader} htmlType="submit">
        Пополнить
      </Button>
    </form>
  );
}

export default React.memo(reduxForm<IReplenishAccount, ILoaderToForm>({form: 'replenishForm'})(ReplenishForm))
