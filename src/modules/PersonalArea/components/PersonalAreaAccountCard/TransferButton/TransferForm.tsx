import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import { AInput } from "../../../../../common/combineAntd";
import { required, number, normalizeAccount, normalizeValue } from '../../../validators/PersonalAreaValidators'
import { Button } from 'antd';
import { ITransferAccount, ILoaderToForm } from "../../../types/personalAreaTypes";

const TransferForm: React.FC<InjectedFormProps<ITransferAccount, ILoaderToForm> & ILoaderToForm> = (props) => {
  const { handleSubmit, pristine, submitting, loader} = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field label="" name="accountNumberReceiver" validate={[required]} normalize={normalizeAccount} component={AInput} placeholder="Номер счета для перевода" hasFeedback/>
      <Field label="" name="value" validate={[required, number]} normalize={normalizeValue} component={AInput} placeholder="Сумма перевода" hasFeedback/>
      <Button type="primary" disabled={pristine || submitting} loading={loader} htmlType="submit">
        Перевод
      </Button>
    </form>
  );
}

export default React.memo(reduxForm<ITransferAccount, ILoaderToForm>({form: 'transferForm'})(TransferForm))
