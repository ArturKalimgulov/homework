import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import TransferForm from './TransferForm';
import {Button, Modal, message } from 'antd';
import { transferAccountLoaderSelector, transferAccountModalShow} from '../../../selectors/PersonalAreaSelectors';
import { transferModalHide, transferModalShow, transferAccountLoader } from '../../../actions';
import {reset} from 'redux-form';
import { ITransferAccount, IPersonalAreaCardProps } from '../../../types/personalAreaTypes';

export const TransferButton = (props: IPersonalAreaCardProps) => {
    const { account } = props;
    const { accountNumber, accountBalance, id } = account;
    const dispatch = useDispatch();
    const visible = useSelector(transferAccountModalShow);
    const loader = useSelector(transferAccountLoaderSelector);
    const handleOk = (values: ITransferAccount) => {
        if (accountBalance >= values.value)
            {dispatch(transferAccountLoader({
                    value: values.value, 
                    accountNumberReceiver: values.accountNumberReceiver.replace(/\s/g, ''),
                    accountNumberCurrent: accountNumber,
            }, id));
            dispatch(reset('transferForm'));
        }
        else
            message.warning('Недостаточно средств для совершения перевода')

    }
    const handleCancel = () => {
        dispatch(transferModalHide());
    }
    const onTransferClick = () => {
        dispatch(transferModalShow())
    }
    return  <>
                <Button onClick={onTransferClick}>Перевод</Button>
                <Modal
                    visible={visible}
                    title="Перевод"
                    onCancel={handleCancel}
                    footer={[
                    ]}
                    >
                    <TransferForm loader={loader} onSubmit={handleOk}/>
                </Modal>
            </>
} 