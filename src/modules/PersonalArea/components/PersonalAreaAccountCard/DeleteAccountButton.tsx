import React from 'react';
import {useDispatch} from 'react-redux';
import {Button, Modal} from 'antd';
import { deleteAccountLoader } from '../../actions';
import { IPersonalAreaCardProps } from '../../types/personalAreaTypes';

const { confirm } = Modal;

export const DeleteAccountButton = (props: IPersonalAreaCardProps) => {
    const { account } = props;
    const { id, accountBalance } = account;
    const dispatch = useDispatch();
    const showConfirm = () => {
        confirm({
          title: 'Вы точно желаете закрыть счет?',
          okText: 'Да',
          cancelText: 'Нет',
          onOk() {
            if (accountBalance === 0){
              dispatch(deleteAccountLoader(id));
            } else {
              confirm({
                title: "Остались средства на счету.",
                okText: 'Закрыть счет',
                cancelText: 'Отмена',
                onOk(){
                  dispatch(deleteAccountLoader(id));
                }
              })
            }
          },
          onCancel() {
          },
        });
      }
    return <Button onClick={showConfirm}>Закрыть счет</Button>
} 