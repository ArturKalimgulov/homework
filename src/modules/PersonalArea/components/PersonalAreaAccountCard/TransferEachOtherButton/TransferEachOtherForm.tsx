import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import { AInput, ASelect } from "../../../../../common/combineAntd";
import { required, number, normalizeValue } from '../../../validators/PersonalAreaValidators'
import { Button, Select } from 'antd';
import { ITransferAccount, IAccount, ITransferEOForm } from "../../../types/personalAreaTypes";
const { Option } = Select;

const TransferEachOtherForm: React.FC<InjectedFormProps<ITransferAccount, ITransferEOForm> & ITransferEOForm> = React.memo((props) => {
  const { handleSubmit, pristine, submitting, loader, accounts} = props;
  return (
    <form onSubmit={handleSubmit}>
        <Field label="Счет списания" name="accountNumberCurrent" component={ASelect} defaultValue={accounts.length > 0 ? accounts[0].accountNumber: ""} placeholder="" >
            { 
              accounts.map((element: IAccount) => {
                return <Option key={element.accountNumber} value={element.accountNumber}>{element.accountNumber}</Option>
              }) 
            }
        </Field>
        <Field label="Счет зачисления" name="accountNumberReceiver" component={ASelect} defaultValue={accounts.length > 0 ? accounts[0].accountNumber: ""}  placeholder="" >
            { 
              accounts.map((element: IAccount) => {
                return <Option key={element.accountNumber} value={element.accountNumber}>{element.accountNumber}</Option>
              }) 
            }
        </Field>
      <Field label="" name="value" validate={[required, number]} normalize={normalizeValue} component={AInput} placeholder="Сумма" hasFeedback/>
      <Button type="primary" disabled={pristine || submitting} loading={loader} htmlType="submit">
        Перевод
      </Button>
    </form>
  );
})

export default reduxForm<ITransferAccount, ITransferEOForm>({form: 'transferEachOtherForm'})(TransferEachOtherForm)
