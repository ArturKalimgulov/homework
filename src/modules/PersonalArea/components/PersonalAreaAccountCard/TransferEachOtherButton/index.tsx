import React from 'react';
import {useDispatch, useSelector} from 'react-redux';
import TransferEachOtheForm from './TransferEachOtherForm';
import {Button, Modal, message } from 'antd';
import { transferEachOtherAccountLoaderSelector, transferEachOtherAccountModalShowSelector} from '../../../selectors/PersonalAreaSelectors';
import { transferEachOtherModalHide, transferEachOtherModalShow, transferEachOtherAccountLoader } from '../../../actions';
import {reset} from 'redux-form';
import { ITransferAccount, IAccount, IPersonalAreaProps } from '../../../types/personalAreaTypes';


export const TransferEachOtherButton = (props: IPersonalAreaProps) => {
    const { accounts, currentAccount } = props;
    const { id } = currentAccount;
    const dispatch = useDispatch();
    const visible = useSelector(transferEachOtherAccountModalShowSelector);
    const loader = useSelector(transferEachOtherAccountLoaderSelector);
    const checkAccount = (accountNumberCurrent: string, value: number) => {
        const currentAccount: IAccount | undefined = accounts.find((el: IAccount) => el.accountNumber === accountNumberCurrent);
        if (currentAccount && currentAccount?.accountBalance >= value)
                return true; 
        return false;
    }
    const handleOk = (values: ITransferAccount) => {
        if (checkAccount(values.accountNumberCurrent, values.value))
            {dispatch(transferEachOtherAccountLoader({
                    ...values,
            }, id));
            dispatch(reset('transferForm'));
        }
        else
            message.warning('Недостаточно средств для совершения перевода')
    }
    const handleCancel = () => {
        dispatch(transferEachOtherModalHide());
    }
    const onTransferClick = () => {
        dispatch(transferEachOtherModalShow())
    }
    return  <>
                <Button onClick={onTransferClick}>Перевод между своими счетами</Button>
                <Modal
                    visible={visible}
                    title="Перевод между своими счетами"
                    onCancel={handleCancel}
                    footer={[
                    ]}
                    >
                    <TransferEachOtheForm accounts={accounts} loader={loader} onSubmit={handleOk}/>
                </Modal>
            </>
} 