import React from 'react';
import {useSelector } from 'react-redux';
import { Typography, Layout, Space, Skeleton } from 'antd';
import { currentAccountLoader } from '../../selectors/PersonalAreaSelectors'
import {DeleteAccountButton} from './DeleteAccountButton';
import {CreateTemplateButton} from './CreateTemplateButton';
import {PaymentButton} from './PaymentButton';
import {ReplenishAccountButton} from './ReplenishAccountButton';
import {TransferButton} from './TransferButton';
import {StatementButton} from './StatementButton';
import {TransferEachOtherButton} from './TransferEachOtherButton';
import { IPersonalAreaProps } from '../../types/personalAreaTypes';
const { Content } = Layout;
const { Text } = Typography;

export default (props: IPersonalAreaProps) => {
    const { accounts, currentAccount } = props;
    const loader = useSelector(currentAccountLoader);

    if (loader)
        return <Content><Skeleton active /></Content>
    return(
        <Content>
            {
                currentAccount ? <div className="PersonalAreaAccountCard_content">
                <div>
                    <Text>Счет №: {currentAccount.accountNumber}</Text>
                    <Text type="secondary">Баланс: {currentAccount.accountBalance}</Text>
                </div>
                <div>
                    <Space size='middle'>
                        <ReplenishAccountButton account={currentAccount}/>
                        <TransferButton account={currentAccount}/>
                        <TransferEachOtherButton currentAccount={currentAccount} accounts={accounts}/>
                        <PaymentButton account={currentAccount}/>
                    </Space>
                </div>
                <div>
                    <Space size='large'>
                        <StatementButton account={currentAccount}/>
                        <CreateTemplateButton account={currentAccount}/>
                        <DeleteAccountButton account={currentAccount}/>
                    </Space>
                </div>
                </div> : <div className="PersonalAreaAccountCard_content">
                            <Text>У вас нет открытых счетов :(</Text>
                            <Text>Нажмите на плюс справа, чтобы завести счет</Text>
                        </div>
            }
        </Content>
    )
}