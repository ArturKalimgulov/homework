import React from 'react';
import { useDispatch } from 'react-redux';
import {setCurrentAccount} from '../../actions/index'
import { Card } from 'antd';
import classNames from 'classnames';
import { checkAccountBalance } from '../../selectors/PersonalAreaSelectors';

export default (props:any) => {
  const dispatch = useDispatch();
  const { data, index, currentId } = props;
  const { accountBalance, accountNumber, id } = data;
  const onClick = () => {
    dispatch(setCurrentAccount(index));
  }
  const accountClass = classNames({
    'ant-card': currentId !== id,
    'ant-card current': currentId === id 
  });

  return (
    <Card title={`Счет: ${accountNumber}`} className={accountClass} bordered={true} onClick={onClick}>
      Баланс: {checkAccountBalance(accountBalance)}
    </Card> 
  )
}
