import React from "react";
import { Field, reduxForm, InjectedFormProps } from "redux-form";
import { AInputPassword, AInput } from "../../../../../../common/combineAntd";
import { required, minLength3, email} from '../../../../validators/PersonalAreaValidators'
import { Button } from 'antd';
import { IChangeUserProfile, ILoaderToForm } from "../../../../types/personalAreaTypes";

const ChangeUserProfileForm: React.FC<InjectedFormProps<IChangeUserProfile, ILoaderToForm> & ILoaderToForm> = React.memo((props) => {
  const { pristine, submitting, handleSubmit, loader} = props;
  return (
    <form onSubmit={handleSubmit}>
      <Field label="" name="firstName" validate={[minLength3]} component={AInput} placeholder="Введите новое имя" hasFeedback/>
      <Field label="" name="newEmail" validate={[email]} component={AInput} placeholder="Введите новый email" hasFeedback/>
      <Field label="" name="password" validate={[required]} component={AInputPassword} placeholder="Подтвердите пароль" hasFeedback/>
      <Button type="primary" disabled={pristine || submitting} loading={loader} htmlType="submit">
        Изменить профиль
      </Button>
    </form>
  );
})

export default reduxForm<IChangeUserProfile, ILoaderToForm>({form: 'changeUserProfile'})(ChangeUserProfileForm)
