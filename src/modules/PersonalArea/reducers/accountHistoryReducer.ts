import produce from 'immer';
import { ACCOUNT_HISTORY_ERROR, ACCOUNT_HISTORY_SUCCESS, ACCOUNT_HISTORY_LOADER, ACCOUNT_HISTORY_FILTER } from '../actions';
import moment from 'moment';
import { IAccountHistoryStore, IHistory } from '../types/personalAreaTypes';
import { PersonalAreaActions } from '../actionTypes';

const initialState: IAccountHistoryStore = {
    loader: false,
    success: false,
    history: [],
    filteredHistory: []
};

export default (state = initialState, action: PersonalAreaActions) => {
    return produce(state, draft => {
        if (action.type === ACCOUNT_HISTORY_LOADER){
            draft.loader = true;
            draft.success = false;
            draft.history = [];
            draft.filteredHistory = [];
        }
        if (action.type === ACCOUNT_HISTORY_SUCCESS){
            draft.loader = false;
            draft.success = true;
            draft.history = [...action.payload];
            draft.filteredHistory = [...action.payload];
        }
        if (action.type === ACCOUNT_HISTORY_ERROR){
            draft.loader = false;
            draft.success = false;
            draft.history = [];
            draft.filteredHistory = [];
        }
        if (action.type === ACCOUNT_HISTORY_FILTER){
            draft.filteredHistory = []
            draft.history.forEach((el: IHistory) => {
                if (moment(el.date).isBetween(action.payload.min, action.payload.max)){
                        draft.filteredHistory.push(el);
                    }
            })
        }

    })
}