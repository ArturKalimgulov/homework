import { call, put, take, delay } from "redux-saga/effects";
import { transferAccountRequest } from "./apiRequests";
import { TRANSFER_EACH_OTHER_ACCOUNT_LOADER,
  transferEachOtherAccountSuccess,
  transferEachOtherAccountError,
  isAccountsListTransferAccount,
  accountHistoryLoader
} from "../actions";
import {message} from 'antd';
import { ITransferAccount } from "../types/personalAreaTypes";

function* transferEachOtherAccountFlow({ accountNumberCurrent, accountNumberReceiver, value }: ITransferAccount, accountId: string) {
  try {
    const response = yield call(transferAccountRequest, accountNumberCurrent, accountNumberReceiver, value);
    yield delay(500);
    yield put(transferEachOtherAccountSuccess());
    yield put(isAccountsListTransferAccount(response.data))
    yield put(accountHistoryLoader(accountId));
    message.success('Перевод прошел успешно', 1.5)
  } catch (error) {
    yield put(transferEachOtherAccountError());
    message.error('Ошибка!', 2.5)
  }
}

function* transferEachOtherAccountSagas() {
  while (true){
    const { payload, accountId } = yield take(
      [TRANSFER_EACH_OTHER_ACCOUNT_LOADER],
    );
    yield call(transferEachOtherAccountFlow, payload, accountId)
  }
}

export default transferEachOtherAccountSagas;
