import { call, put, take, delay } from "redux-saga/effects";
import { replenishAccountRequest } from "./apiRequests";
import { REPLENISH_ACCOUNT_LOADER,
  replenishAccountSuccess,
  replenishAccountError,
  isAccountsListReplenishAccount,
  accountHistoryLoader
} from "../actions";
import {message} from 'antd';
import { IReplenishAccount } from "../types/personalAreaTypes";

function* replenishAccountFlow({ accountNumber, value }: IReplenishAccount) {
  try {
    const response = yield call(replenishAccountRequest, accountNumber, value);
    yield delay(500);
    yield put(replenishAccountSuccess());
    yield put(isAccountsListReplenishAccount(response.data.value, response.data.id));
    yield put(accountHistoryLoader(response.data.id));
    message.success('Пополнение успешно', 1.5)
  } catch (error) {
    yield put(replenishAccountError());
    message.error('Ошибка!', 2.5)
  }
}

function* replenishAccountSagas() {
  while (true){
    const { payload } = yield take(
     [REPLENISH_ACCOUNT_LOADER],
    );
    yield call(replenishAccountFlow, payload);
  }
}

export default replenishAccountSagas;
