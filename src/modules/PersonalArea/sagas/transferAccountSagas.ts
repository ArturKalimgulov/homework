import { call, put, take, delay } from "redux-saga/effects";
import { transferAccountRequest } from "./apiRequests";
import { TRANSFER_ACCOUNT_LOADER,
  transferAccountSuccess,
  transferAccountError,
  isAccountsListTransferAccount,
  accountHistoryLoader
} from "../actions";
import {message} from 'antd';
import { ITransferAccount } from "../types/personalAreaTypes";

function* transferAccountFlow({ value, accountNumberCurrent, accountNumberReceiver }: ITransferAccount, accountId: string) {
  try {
    const response = yield call(transferAccountRequest, accountNumberCurrent, accountNumberReceiver, value);
    yield delay(500);
    yield put(transferAccountSuccess());
    yield put(isAccountsListTransferAccount(response.data))
    yield put(accountHistoryLoader(accountId))
    message.success('Перевод прошел успешно', 1.5)
  } catch (error) {
    yield put(transferAccountError());
    message.error('Ошибка!', 2.5)
  }
}

function* transferAccountSagas() {
  while (true) {
    const { payload, accountId } = yield take(
      [TRANSFER_ACCOUNT_LOADER],
    );
    yield call(transferAccountFlow, payload, accountId);
  }
}

export default transferAccountSagas;
