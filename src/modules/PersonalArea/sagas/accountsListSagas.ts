import { call, put, take, delay, fork, select, cancel } from "redux-saga/effects";
import { accountsListRequest } from "./apiRequests";
import {
  ISACCOUNTSLIST_LOADER,
  SET_CURRENT_ACCOUNT,
  setCurrentAccountSuccess,
  setCurrentAccountError,
  isAccountsListError,
  isAccountsListSuccess,
  accountHistoryLoader
} from "../actions";
import { currentAccountValueSelector } from '../selectors/PersonalAreaSelectors';

function* accountsListFlow(value: number) {
  try {
    const userId = localStorage.getItem('userId');
    const response = yield call(accountsListRequest, userId);
    yield delay(500);
    yield put(isAccountsListSuccess(response.data));
    yield put(setCurrentAccountSuccess(value ? value: 0));
    const accountId = yield select(currentAccountValueSelector);
    yield put(accountHistoryLoader(accountId));
  } catch (error) {
    yield put(isAccountsListError());
    yield put(setCurrentAccountError());
  }
}

function* accountsListSagas() {
    const { value } = yield take(ISACCOUNTSLIST_LOADER);
    yield call(accountsListFlow, value);
    while (true){
      const { value } = yield take(SET_CURRENT_ACCOUNT);
      yield call(accountsListFlow, value)
    }
}

export default accountsListSagas;
