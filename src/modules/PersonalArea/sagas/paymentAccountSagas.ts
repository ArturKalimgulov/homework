import { call, put, take, delay } from "redux-saga/effects";
import { paymentAccountRequest } from "./apiRequests";
import { PAYMENT_ACCOUNT_LOADER,
  paymentAccountSuccess,
  paymentAccountError,
  isAccountsListTransferAccount,
  accountHistoryLoader
} from "../actions";
import {message} from 'antd';
import { IPaymentAccount } from "../types/personalAreaTypes";

function* paymentAccountFlow({ accountNumberCurrent, accountNumberReceiver, value, useTemplate }: IPaymentAccount, accountId: string) {
  try {
    const response = yield call(paymentAccountRequest, accountNumberCurrent, accountNumberReceiver, value, useTemplate);
    yield delay(500);
    yield put(paymentAccountSuccess());
    yield put(isAccountsListTransferAccount(response.data))
    yield put(accountHistoryLoader(accountId));
    message.success('Платеж совершен', 1.5)
  } catch (error) {
    yield put(paymentAccountError());
    message.error('Ошибка! Платеж не совершен.', 2.5)
  }
}

function* paymentAccountSagas() {
  while (true){
    const { payload, accountId } = yield take(
     [PAYMENT_ACCOUNT_LOADER],
    );
    yield call(paymentAccountFlow, payload, accountId)
  }
}

export default paymentAccountSagas;
