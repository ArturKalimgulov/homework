import { call, put, take, delay, select } from "redux-saga/effects";
import { deleteAccountRequest } from "./apiRequests";
import { DELETE_ACCOUNT_LOADER, 
  isAccountsListDeleteAccount,
  deleteAccountSuccess,
  deleteAccountError,
  setCurrentAccountIndex,
  accountHistoryLoader
} from "../actions";
import { lastAccountIdSelector, lastAccountIndexSelector } from '../selectors/PersonalAreaSelectors';
import {message} from 'antd';

function* deleteAccountFlow(id: string) {
  try {
    yield call(deleteAccountRequest, id);
    yield delay(500);
    yield put(deleteAccountSuccess());
    const lastIndex = yield select(lastAccountIndexSelector);
    yield put(setCurrentAccountIndex(lastIndex));
    yield put(isAccountsListDeleteAccount(id));
    const lastId = yield select(lastAccountIdSelector);
    if (lastId) yield put(accountHistoryLoader(lastId));
    message.success('Счет закрыт', 1.5)
  } catch (error) {
    yield put(deleteAccountError());
    message.error('Ошибка!', 2.5)
  }
}

function* deleteAccountSagas() {
  while (true){
    const { id } = yield take(
     [DELETE_ACCOUNT_LOADER],
    );
    yield call(deleteAccountFlow, id)
  }
}

export default deleteAccountSagas;
