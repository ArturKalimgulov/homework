import axios from 'axios';
import { IHistory, IUserId, IAccount } from '../types/personalAreaTypes';


export const accountsListRequest = (userId: IUserId): Promise<IAccount[]> => {
    return axios.get(`api/AccountsModels/${userId}`,{})
}
export const newAccountRequest = (userId: IUserId) => {
    return axios.post(`api/AccountsModels/`,{
        userId 
    })
}
export const deleteAccountRequest = (id: string) => {
    return axios.delete(`api/AccountsModels/${id}`)
}
export const replenishAccountRequest = (accountNumber: string, value: number) => {
    return axios.post(`api/ReplenishAccounts/`,{
        accountNumber,
        value
    })
}
export const transferAccountRequest = (accountNumberCurrent: string, accountNumberReceiver: string, value: number) => {
    return axios.post(`api/TransferAccounts/`,{
        accountNumberCurrent,
        accountNumberReceiver,
        value 
    });
}
export const paymentAccountRequest = (accountNumberCurrent: string, accountNumberReceiver: string, value: number, useTemplate: boolean) => {
    return axios.post(`api/PaymentAccounts`,{
        accountNumberCurrent,
        accountNumberReceiver,
        value,
        useTemplate 
    });
}
export const changeUserPasswordRequest = (email: string , oldPassword: string, newPassword: string): Promise<object> => {
    return axios.post(`changePassword`, 
    {
        email,
        oldPassword,
        newPassword
    })
}
export const changeUserProfileRequest = (oldEmail: string, newEmail: string, password: string, firstName: string, fileName: string) => {
    return axios.post( `updateUser`,
    {
        oldEmail,
        newEmail,
        password,
        firstName,
        fileName
    })
}
export const deleteUserProfileRequest = (userId: IUserId) => {
    return axios.delete(`deleteUser/${userId}`)
}
export const createTemplateRequest = (values: any) => {
    return axios.post(`api/TemplatesModels/`,{
        ...values
    })
}
export const getTemplateRequest = (id: string) => {
    return axios.get(`api/TemplatesModels/${id}`)
}
export const changeTemplateRequest = (id: string, values: any) => {
    return axios.put(`api/TemplatesModels/${id}`,{
        ...values
    })
}
export const getHistoryRequest = (id: string): Promise<IHistory[]> =>{
    return axios.get(`api/HistoryModels/${id}`)
}