import { call, put, take, delay } from "redux-saga/effects";
import { newAccountRequest } from "./apiRequests";
import { NEW_ACCOUNT_LOADER,
  newAccountSuccess,
  newAccountError,
  isAccountsListNewAccount,
  setCurrentAccountIndex, 
  accountHistoryLoader
} from "../actions";
import { message } from 'antd'

function* newAccountFlow(value: number) {
  try {
    const userId = localStorage.getItem('userId');
    const response = yield call(newAccountRequest, userId);
    yield delay(500);
    yield put(newAccountSuccess());
    yield put(isAccountsListNewAccount(response.data));
    yield put(setCurrentAccountIndex(value));
    yield put(accountHistoryLoader(response.data.id));
    message.success('Счет открыт успешно!', 1.5)
  } catch (error) {
    yield put(newAccountError());
    message.error('Ошибка!', 2.5)
  }
}

function* newAccountSagas() {
  while (true){
    const { value } = yield take(
     [NEW_ACCOUNT_LOADER],
    );
    yield call(newAccountFlow, value)
  }
}

export default newAccountSagas;
