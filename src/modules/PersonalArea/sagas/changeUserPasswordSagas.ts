import { call, put, take, delay } from "redux-saga/effects";
import { changeUserPasswordRequest } from "./apiRequests";
import { CHANGE_USER_PASSWORD_LOADER,
  changeUserPasswordSuccess,
  changeUserPasswordError,
} from "../actions";
import {message} from 'antd';
import { IChangePassword } from "../types/personalAreaTypes";

function* changeUserPasswordFlow({ newPassword, email, oldPassword }: IChangePassword) {
  try {
    yield call(changeUserPasswordRequest, email, oldPassword, newPassword);
    yield delay(500);
    yield put(changeUserPasswordSuccess());
    message.success('Пароль успешно изменен', 1.5)
  } catch (error) {
    yield put(changeUserPasswordError());
    message.error(`${error.response.data.message}`, 2.5)
  }
}

function* changeUserPasswordSagas() {
  while (true){
    const { payload } = yield take(
      [CHANGE_USER_PASSWORD_LOADER],
    );
    yield call(changeUserPasswordFlow, payload);
  }
}

export default changeUserPasswordSagas;
