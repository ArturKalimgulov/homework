import { call, put, take, delay } from "redux-saga/effects";
import { changeUserProfileRequest } from "./apiRequests";
import { CHANGE_USER_PROFILE_LOADER,
  changeUserProfileSuccess,
  changeUserProfileError,
  getUser
} from "../actions";
import {message} from 'antd';
import { IChangeUserProfile } from "../types/personalAreaTypes";

function* changeUserProfileFlow({ password, firstName, newEmail, oldEmail, fileName }: IChangeUserProfile) {
  try {
    const userId = localStorage.getItem('userId');
    yield call(changeUserProfileRequest, oldEmail, newEmail, password, firstName, fileName);
    yield delay(500);
    yield put(changeUserProfileSuccess());
    yield put(getUser(userId));
    message.success('Профиль успешно изменен', 1.5)
  } catch (error) {
    yield put(changeUserProfileError());
    message.error(`${error.response.data.message}`, 2.5)
  }
}

function* changeUserProfileSagas() {
  while (true){
    const { payload } = yield take(
      [CHANGE_USER_PROFILE_LOADER],
    );
    yield call(changeUserProfileFlow, payload)
  }
}

export default changeUserProfileSagas;
