import * as actions from '../actions/'
import * as types from '../types/personalAreaTypes'


export interface accountHistoryLoaderAction {
    type: typeof actions.ACCOUNT_HISTORY_LOADER,
    id: string
}
export interface accountHistoryFilterAction {
    type: typeof actions.ACCOUNT_HISTORY_FILTER,
    payload: types.IFilterHistory
}
export interface accountHistorySuccessAction {
    type: typeof actions.ACCOUNT_HISTORY_SUCCESS,
    payload: types.IHistory[]
}
export interface isAccountsListSuccessAction {
    type: typeof actions.ISACCOUNTSLIST_SUCCESS,
    accounts: types.IAccount[]
}
export interface isAccountsListNewAccountAction {
    type: typeof actions.ISACCOUNTSLIST_NEW_ACCOUNT,
    account: types.IAccount
}
export interface isAccountsListReplenishAccountAction {
    type: typeof actions.ISACCOUNTSLIST_REPLENISH_ACCOUNT,
    value: number,
    id: string
}
export interface isAccountsListDeleteAccountAction {
    type: typeof actions.ISACCOUNTSLIST_DELETE_ACCOUNT,
    id: string
}
export interface isAccountsListTransferAccountAction {
    type: typeof actions.ISACCOUNTSLIST_TRANSFER_ACCOUNT,
    payload: types.IGetResultTransaction
}
export interface setCurrentAccountAction {
    type: typeof actions.SET_CURRENT_ACCOUNT | typeof actions.SET_CURRENT_ACCOUNT_SUCCESS | typeof actions.SET_CURRENT_ACCOUNT_INDEX,
    value: number
}
export interface changeUserPasswordLoaderAction {
    type: typeof actions.CHANGE_USER_PASSWORD_LOADER,
    payload: types.IChangePassword
}
export interface changeUserProfileLoaderAction {
    type: typeof actions.CHANGE_USER_PROFILE_LOADER,
    payload: types.IChangeUserProfile
}
export interface changeUserProfileSetAvatarAction {
    type: typeof actions.CHANGE_USER_PROFILE_SET_AVATAR,
    imageCode: string
}
export interface createTemplateLoaderAction {
    type: typeof actions.CREATE_TEMPLATE_LOADER,
    payload: types.IPaymentAccount
}
export interface getTemplateLoaderAction {
    type: typeof actions.GET_TEMPLATE_LOADER,
    id: types.IPaymentAccount
}
export interface getTemplateSuccessAction {
    type: typeof actions.GET_TEMPLATE_SUCCESS,
    payload: types.IPaymentAccount
}
export interface deleteAccountLoaderAction {
    type: typeof actions.DELETE_ACCOUNT_LOADER,
    id: types.IAccount
}
export interface newAccountLoaderAction {
    type: typeof actions.NEW_ACCOUNT_LOADER,
    value: number
}
export interface paymentAccountLoaderAction {
    type: typeof actions.PAYMENT_ACCOUNT_LOADER,
    payload: types.IPaymentAccount
}
export interface replenishAccountLoaderAction {
    type: typeof actions.REPLENISH_ACCOUNT_LOADER,
    payload: types.IReplenishAccount
}
export interface transferAccountLoaderAction {
    type: typeof actions.TRANSFER_ACCOUNT_LOADER,
    payload: types.ITransferAccount
}
export interface transferEachOtherAccountLoaderAction {
    type: typeof actions.TRANSFER_EACH_OTHER_ACCOUNT_LOADER,
    payload: types.ITransferAccount
}

  export interface onlyTypeAction {
    type: 
      typeof actions.ISACCOUNTSLIST_LOADER | typeof actions.ISACCOUNTSLIST_ERROR 
    | typeof actions.SET_CURRENT_ACCOUNT_ERROR | typeof actions.NEW_ACCOUNT_SUCCESS 
    | typeof actions.NEW_ACCOUNT_ERROR | typeof actions.DELETE_ACCOUNT_SUCCESS 
    | typeof actions.DELETE_ACCOUNT_ERROR | typeof actions.REPLENISH_ACCOUNT_SUCCESS
    | typeof actions.REPLENISH_ACCOUNT_ERROR | typeof actions.REPLENISH_ACCOUNT_MODAL_SHOW 
    | typeof actions.REPLENISH_ACCOUNT_MODAL_HIDE | typeof actions.TRANSFER_ACCOUNT_SUCCESS 
    | typeof actions.TRANSFER_ACCOUNT_ERROR | typeof actions.TRANSFER_ACCOUNT_MODAL_SHOW 
    | typeof actions.TRANSFER_ACCOUNT_MODAL_HIDE | typeof actions.TRANSFER_EACH_OTHER_ACCOUNT_ERROR 
    | typeof actions.TRANSFER_EACH_OTHER_ACCOUNT_MODAL_SHOW | typeof actions.TRANSFER_EACH_OTHER_ACCOUNT_MODAL_HIDE 
    | typeof actions.CHANGE_USER_PASSWORD_SUCCESS | typeof actions.CHANGE_USER_PASSWORD_ERROR 
    | typeof actions.CHANGE_USER_PASSWORD_MODAL_HIDE | typeof actions.CHANGE_USER_PASSWORD_MODAL_SHOW 
    | typeof actions.CHANGE_USER_PROFILE_SUCCESS | typeof actions.CHANGE_USER_PROFILE_ERROR 
    | typeof actions.CHANGE_USER_PROFILE_MODAL_HIDE | typeof actions.CHANGE_USER_PROFILE_MODAL_SHOW 
    | typeof actions.DELETE_USER_PROFILE_LOADER | typeof actions.DELETE_USER_PROFILE_SUCCESS 
    | typeof actions.DELETE_USER_PROFILE_ERROR | typeof actions.PAYMENT_ACCOUNT_SUCCESS 
    | typeof actions.PAYMENT_ACCOUNT_ERROR | typeof actions.PAYMENT_ACCOUNT_MODAL_HIDE 
    | typeof actions.PAYMENT_ACCOUNT_MODAL_SHOW | typeof actions.CREATE_TEMPLATE_SUCCESS 
    | typeof actions.CREATE_TEMPLATE_ERROR | typeof actions.CREATE_TEMPLATE_MODAL_HIDE 
    | typeof actions.CREATE_TEMPLATE_MODAL_SHOW | typeof actions.GET_TEMPLATE_ERROR 
    | typeof actions.ACCOUNT_HISTORY_ERROR | typeof actions.TRANSFER_EACH_OTHER_ACCOUNT_SUCCESS

  }
  
  

  
  export type PersonalAreaActions =
    | onlyTypeAction
    | accountHistorySuccessAction
    | accountHistoryFilterAction
    | accountHistoryLoaderAction
    | isAccountsListSuccessAction
    | isAccountsListNewAccountAction
    | isAccountsListReplenishAccountAction
    | isAccountsListDeleteAccountAction
    | isAccountsListTransferAccountAction
    | setCurrentAccountAction
    | changeUserPasswordLoaderAction
    | changeUserProfileLoaderAction
    | changeUserProfileSetAvatarAction
    | createTemplateLoaderAction
    | getTemplateLoaderAction
    | getTemplateSuccessAction
    | deleteAccountLoaderAction
    | newAccountLoaderAction
    | paymentAccountLoaderAction
    | replenishAccountLoaderAction
    | transferAccountLoaderAction
    | transferEachOtherAccountLoaderAction