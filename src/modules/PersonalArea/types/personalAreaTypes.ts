export interface IAccountHistoryStore {
    loader: boolean,
    success: boolean,
    history: IHistory[],
    filteredHistory: IHistory[]
}
export interface IAccountListStore {
    isAccountsLoader: boolean,
    isAccountsSuccess: boolean,
    accounts: IAccount[]
}
export interface IAccountStore {
    isCurrentAccountLoader: boolean,
    isCurrentAccountSuccess: boolean,
    currentAccount: number
}
export interface IChangeUserPasswordStore {
    isModalShow: boolean,
    loader: boolean,
    success: boolean,
}
export interface IChangeUserProfileStore {
    isModalShow: boolean,
    loader: boolean,
    success: boolean,
    profilePicture: string | null
}
export interface ICreateTemplateStore {
    isModalShow: boolean,
    loader: boolean,
    success: boolean,
    template: IPaymentAccount | undefined
}
export interface IDeleteAccountStore {
    isDeleteAccountLoader: boolean,
    isDeteteAccountSuccess: boolean,
}
export interface IDeleteUserProfileStore {
    loader: boolean,
    success: boolean,
}
export interface INewAccountStore {
    isNewAccountLoader: boolean,
    isNewAccountSuccess: boolean,
}
export interface IPaymentAccountStore {
    isModalShow: boolean,
    loader: boolean,
    success: boolean,
}
export interface IReplenishAccountStore {
    isModalShow: boolean,
    isReplenishAccountLoader: boolean,
    isReplenishAccountSuccess: boolean,
}
export interface ITransferAccountStore {
    isModalShow: boolean,
    isTransferAccountLoader: boolean,
    isTransferAccountSuccess: boolean,
}
export interface ITransferEachOtherAccountStore {
    isModalShow: boolean,
    loader: boolean,
    success: boolean,
}

export interface IFilterHistory {
    min: string,
    max: string,
}

export type IUserId = string | null;

export interface ILoaderToForm{
    loader: boolean
}
export interface ITransferEOForm{
    loader: boolean,
    accounts: IAccount[]
}

export interface IGetResultTransaction{
    current: IAccount,
    receiver: IAccount
}


export interface IHistory {
    id: string,
    accountId: string,
    type: string,
    date: string,
    value: number
}

export interface IAccount {
    id: string,
    accountNumber: string,
    accountBalance: number,
    dateCreated: string,
    userId: string,
    status: boolean
}
export interface IUser {
    id: string,
    firstName: string,
    email: string,
    profilePicture: string,
    isClient: boolean,
    status: boolean
}

export interface IChangePassword {
    email: string,
    oldPassword: string,
    newPassword: string,
    confrmNewPassword: string
}
export interface IPaymentAccount {
    id: string,
    accountNumberReceiver: string,
    accountNumberCurrent: string,
    value: number,
    useTemplate: boolean,
    paymentName: string,
    receiverName: string,
    receiverEmail: string,
    paymentPurpose: string,
}
export interface IReplenishAccount {
    accountNumber: string,
    value: number,
}
export interface ITransferAccount {
    accountNumberReceiver: string,
    accountNumberCurrent: string,
    value: number,
}
export interface IChangeUserProfile {
    oldEmail: string,
    newEmail: string,
    password: string,
    firstName: string,
    profileImage: string | null,
    fileName: string,
}
export interface IPersonalAreaProps {
    accounts: IAccount[],
    currentAccount: IAccount
}
export interface IPersonalAreaCardProps {
    account: IAccount
}