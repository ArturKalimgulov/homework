import {createSelector} from 'reselect';
import { IAccountHistoryStore, IPaymentAccountStore, ICreateTemplateStore, IChangeUserPasswordStore, ITransferEachOtherAccountStore, ITransferAccountStore, IReplenishAccountStore, IAccountStore, IAccountListStore, IChangeUserProfileStore } from '../types/personalAreaTypes';
import { userStore } from '../../Login/types/loginTypes';

const accounts = (state: {accounts: IAccountListStore}) => state.accounts;  
const currentAccount = (state: {currentAccount: IAccountStore}) => state.currentAccount;  
const user = (state: {user: userStore}) => state.user;  
const replenishAccount = (state: {replenishAccount: IReplenishAccountStore}) => state.replenishAccount;  
const transferAccount = (state: {transferAccount: ITransferAccountStore}) => state.transferAccount;  
const transferEachOtherAccount = (state: {transferEachOtherAccount: ITransferEachOtherAccountStore}) => state.transferEachOtherAccount;  
const form = (state: any) => state.form;  
const changeUserPassword = (state: {changeUserPassword: IChangeUserPasswordStore}) => state.changeUserPassword;  
const changeUserProfile = (state: {changeUserProfile: IChangeUserProfileStore}) => state.changeUserProfile;  
const paymentAccount = (state: {paymentAccount: IPaymentAccountStore}) => state.paymentAccount;  
const createTemplate = (state: {createTemplate: ICreateTemplateStore}) => state.createTemplate;  
const accountHistory = (state: {accountHistory: IAccountHistoryStore}) => state.accountHistory;  

export const formValueSelector = createSelector(form, form => form);
export const accountsList = createSelector(accounts, accounts => accounts.accounts);
export const userId = createSelector(user, user => user.userId);
export const userSelector = createSelector(user, user => user);
export const accountsLoader = createSelector(accounts, accounts => accounts.isAccountsLoader);
export const accountsLengthSelector = createSelector(accounts, accounts => accounts.accounts.length);
export const accountsBalanceSelector = createSelector(accounts, accounts => checkAccountBalance(accounts.accounts.reduce((sum, current) => sum + current.accountBalance, 0)));
export const currentAccountLoader = createSelector(currentAccount, current => current.isCurrentAccountLoader);
export const currentAccountSelector = createSelector(accounts, currentAccount, (accounts, current) => accounts.accounts[current.currentAccount]);
export const currentAccountValueSelector = createSelector(accounts, currentAccount, (accounts, current) => accounts.accounts[current.currentAccount].id);
export const currentAccountBalanceSelector = createSelector(accounts, currentAccount, (accounts, current) => {
    const account = accounts.accounts[current.currentAccount];
    if (account)
        return checkAccountBalance(account.accountBalance);
    return "0.00"
});
export const lastAccountSelector = createSelector(accounts, (accounts) => accounts.accounts[accounts.accounts.length-2]);
export const lastAccountIdSelector = createSelector(accounts, (accounts) => {
    if (accounts.accounts.length !== 0)
        return accounts.accounts[accounts.accounts.length-1].id
    return null
});
export const lastAccountIndexSelector = createSelector(accounts, (accounts) => accounts.accounts.length-2);
export const transferAccountLoaderSelector = createSelector(transferAccount, transfer => transfer.isTransferAccountLoader);
export const transferAccountModalShow = createSelector(transferAccount, transfer => transfer.isModalShow);
export const transferEachOtherAccountLoaderSelector = createSelector(transferEachOtherAccount, transfer => transfer.loader);
export const transferEachOtherAccountModalShowSelector = createSelector(transferEachOtherAccount, transfer => transfer.isModalShow);
export const replenishAccountLoaderSelector = createSelector(replenishAccount, replenish => replenish.isReplenishAccountLoader);
export const replenishAccountSuccessSelector = createSelector(replenishAccount, replenish => replenish.isReplenishAccountSuccess);
export const replenishAccountModalShow = createSelector(replenishAccount, replenish => replenish.isModalShow);
export const changeUserPasswordModalShowSelector = createSelector(changeUserPassword, store => store.isModalShow);
export const changeUserPasswordLoaderSelector = createSelector(changeUserPassword, store => store.loader);
export const changeUserProfileModalShowSelector = createSelector(changeUserProfile, store => store.isModalShow);
export const changeUserProfileLoaderSelector = createSelector(changeUserProfile, store => store.loader);
export const changeUserProfileAvatarSelector = createSelector(changeUserProfile, store => store.profilePicture);
export const paymentAccountLoaderSelector = createSelector(paymentAccount, store => store.loader);
export const paymentAccountModalShowSelector = createSelector(paymentAccount, store => store.isModalShow);
export const createTemplateModalShowSelector = createSelector(createTemplate, store => store.isModalShow);
export const templateSelector = createSelector(createTemplate, store => store.template);
export const accountHistoryDataSelector = createSelector(accountHistory, store => store.filteredHistory ? [...store.filteredHistory].reverse() : undefined);
export const accountHistoryLoaderSelector = createSelector(accountHistory, store => store.loader);


export const checkAccountBalance = (value: number) => {
    if(!value)
        return "0.00"
    if (value.toString().indexOf('.') === -1)
    return `${value}.00`;
return value;
}